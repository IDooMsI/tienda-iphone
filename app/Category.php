<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $guarded = [];

    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    // public function subcategory(){
    //     return $this->belongsTo(Subcategory::class);
    // }

    public function subcategories()
    {
        return $this->hasMany(Subcategory::class, 'category_id');
    }

}
