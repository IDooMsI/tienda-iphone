<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::all();
        $vac=compact('categories');
        return view('admin.categories.index',$vac);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request);
        Category::create([
            'name'=>$request['name'],
            'description'=>$request['description']
        ]);
        return redirect()->route('categories.index')->with('notice', 'La categoria ha sido creada correctamente');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $vac = compact('category');
        return view('admin.categories.edit',$vac);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator($request);
        $category=Category::find($id);
        $category->update([
            'name'=>$request->input('name'),
            'description'=>$request->input('description')
        ]); 
        return redirect()->route('categories.index')->with('notice', 'La categoria ha sido editada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   

        $category=Category::find($id);
        var_dump($category);
        $category->delete();
        return redirect()->route('categories.index')->with('notice', 'La categoria ha sido eliminada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        $vac = compact('category');
        return view('admin.categories.show', $vac);
    }

    /**
     * @param  null|Request
     */
    public function search(Request $req)
    {
        if($req['buscador']){
            $categories = Category::where('name', "like", "%" . $req['buscador'] . "%")->get();
            $vac = compact('categories');
            return view('admin.categories.results',$vac);
        }
        
        $categories = Category::all();
        $vac = compact('categories');
        return view('admin.categories.results', $vac);
    }
    
    public function validator(Request $request)
    {
        $rules = [
            'name' => 'required|unique:categories|string|max:50',
            'description' => 'required|string|max:300'
        ];
        $message = [
            'required' => 'El campo es obligatorio.',
            'unique' => 'La categoria ya existe en nuestra base.',
            'string' => 'Solo se admiten letras.'
        ];
        return $this->validate($request, $rules, $message);
    }
}
