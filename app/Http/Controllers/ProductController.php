<?php

namespace App\Http\Controllers;

use App\Category;
use App\Color;
use App\Image;
use Illuminate\Http\Request;
use App\Product;
use App\Subcategory;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $vac = compact('products');
        return view('admin.products.index',$vac);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $vac = compact('categories','subcategories');
        return view('admin.products.create', $vac);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destacado = false;
        $discount = false;
        $categoryRep = false;
        $stock = false;
        
        if ($request['category_repuestos']) {
            $categoryRep = $request['category_repuestos'];            
        }
        if(isset($request['destacado'])){
            $destacado = true;
        }
        if (isset($request['sale'])) {
            $discount = $this->discount($request);
        }
        if (isset($request['stock'])) {
            $stock = true;
        }
        $this->validator($request);
        $color = $this->createColor($request);
        
        $product = Product::create([
            'name'=>$request['name'],
            'stock'=>$stock,
            'price'=>$request['price'],
            'sale'=>$request['sale'],
            'discount'=>$discount,
        //     'image'=>$image,
            'description'=>$request['description'],
            'destacado'=>$destacado,
            'category_id'=>$request['category'],
            'colors_id'=>$color->id,
            'subcategory_id'=>$request['subcategory'],
            'categories_repuestos'=>$categoryRep,
        ]);
        $this->createImage($request, $product);

        return redirect()->route('products.index')->with('notice', 'El producto ha sido creado correctamente.');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        $subcategories = Subcategory::all();
        $vac = compact('product','categories','subcategories');
        return view('admin.products.edit',$vac);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        
        $name = $product->name;
        if (isset($request['name'])) {
            $name = $request['name'];
        }

        $price = $product->price;
        if (isset($request['price'])) {
            $price = $request['price'];
        }

        $sale = $product->sale;
        if (isset($request['sale'])) {
            $sale = $request['sale'];
        }

        $discount = $product->discount;
        if (isset($request['sale'])) {
            $discount = $this->discount($request);
        }

        // $image = $product->image;
        // if(isset($request['img'])){
        //     $image = $this->createImage($request);   
        // }

        $description = $product->description;
        if (isset($request['description'])) {
            $description = $request['description'];
        }
        
        $color = $product->colors_id;
        if(isset($request['codigo'])){
            $color = $this->createColor($request);
        }

        $categoryRep = $product->categories_repuestos;
        if ($request['category_repuestos']) {
            $categoryRep = $request['category_repuestos'];
        }

        $stock = false;
        $destacado = false;
        $discount = false;

        if (isset($request['destacado'])) {
            $destacado = true;
        }

        if (isset($request['sale'])) {
            $discount = $this->discount($request);
        }

        if (isset($request['stock'])) {
            $stock = true;
        }
        
        $product->update([
            'name' => $name,
            'stock' => $stock,
            'price' => $price,
            'sale' => $sale,
            'discount' => $discount,
            // 'image' => $image,
            'description' => $description,
            'destacado' => $destacado,
            'category_id' => $request['category'],
            'colors_id' => $color->id,
            'subcategory_id'=>$request['subcategory'],
            'categories_repuestos'=>$categoryRep,
        ]);
        return redirect()->route('products.index')->with('notice', 'El producto ha sido editado correctamente.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product=Product::find($id);
        $product->delete();
        return redirect()->route('products.index')->with('notice', 'El producto ha sido eliminado correctamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $vac = compact('product');
        return view('show', $vac);
    }

    /**
     * @param  null|Request
     */
    public function search(Request $req)
    {
        if ($req['buscador']) {
            $products = Product::where('name', "like", "%" . $req['buscador'] . "%")->get();
            $vac = compact('products');
            return view('admin.products.results', $vac);
        }

        $products = Product::all();
        $vac = compact('products');
        return view('admin.products.results', $vac);
    }

    public function validator(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:50',
            'price' => 'required|numeric',
            'description' => 'required|string|max:500',
            'codigo'=>'required'
        ];
        $message = [
            'required' => 'El campo es obligatorio',
            'unique' => 'El producto ya existe en nuestra base',
            'string' => 'El campo no puede estar vacio',
            'numeric' => 'Solo se admiten números',
        ];
        return $this->validate($request, $rules, $message);
    }

    public function createImage(Request $request, Product $product)
    {
        $position = 0;
        foreach ($request['img'] as $key) {
            $file = $key;
            $name = $request->input('name')."-".$request['name-color'].$position++.".".$file->extension();
            $path = $file->storeAs('products',$name, 'public');
            $image = Image::create([
                'name'=>$path,
                'product_id'=>$product->id,
            ]);
        }
        return $image;
    }

    public function createColor(Request $request)
    {
        $color = Color::create([
            'name'=>$request['name-color'],
            'codigo'=>$request['codigo'],
        ]);
        return $color;
    }

    public function discount(Request $request)
    {
      $price = $request['price'];
      $sale = $request['sale'];
      $resultado = ($price * $sale) / 100;
      $discount = $price - $resultado;
      return $discount; 
    }
}
