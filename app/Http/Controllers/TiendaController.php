<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Subcategory;
use App\Product;

class TiendaController extends Controller
{
   public function buscarProductos(Request $request)
   {
      if ($request['buscador']) {
         $products = Product::where('name', "like", "%" . $request['buscador'] . "%")->get();
         $vac = compact('products');
         return view('results', $vac);
      }
      $products = Product::all();
      $vac = compact('products');
      return view('results', $vac);
   }

   public function showSubcat($subcatName)
   {  
      if ($subcatName == 'nuevo') {
         $subcategory = Subcategory::where('name',$subcatName)->get();
         $subcategory = $subcategory[0];
         $products = Product::where('subcategory_id',$subcategory->id)->get();

      }elseif ($subcatName == 'exhibicion') {
         $subcategory = Subcategory::where('name',$subcatName)->get();
         $subcategory = $subcategory[0];
         $products = Product::where('subcategory_id',$subcategory->id)->get();
      }else{
         $subcategory = Subcategory::where('name',$subcatName)->get();
         $subcategory = $subcategory[0];
         $products = Product::where('subcategory_id',$subcategory->id)->get();
      }
      $vac = compact('subcategory','products');
      return view('categories.show',$vac);
   }

   public function showCat($categoryName)
   {
      if ($categoryName == "repuestos") {
         return redirect()->route('repuestos.all');
      }

      $category = Category::where('name',$categoryName)->get();
      $category = $category[0];
      $products = Product::where('category_id',$category->id)->get();
      $vac = compact('category','products');
      return view('categories.show',$vac);
   
   }

   public function showAll()
   {
      $products = Product::where('category_id',1)->get();
      $vac = compact('products');
      return view('categories.show', $vac);
   }

   public function showRepuestosAll()
   {
      $products = Product::where('category_id',5)->get();
      $vac = compact('products');
      return view('categories.show-repuestos', $vac);
   }
   
   public function showRepuestos($category)
   {
      $products = Product::where('categories_repuestos',$category)->get();
      $vac = compact('products');
      return view('categories.show-repuestos',$vac);
   }
  
}
