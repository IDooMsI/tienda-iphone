<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Product extends Model
{
    public $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class, 'subcategory_id', 'id');
    }

    public function color()
    {
        return $this->belongsTo(Color::class, 'colors_id', 'id');
    }

    public function imagen()
    {
        return $this->hasMany(Image::class);
    }
}
