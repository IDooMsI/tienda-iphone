@extends('layouts.app')
@section('content')
<div id="div-results-cat" class="col-12">
    <div id="div-tit-cat" class="col-12">
        <h2 class="">Resultados de busqueda</h2>
    </div>
    <div id="div-cancel-cat" class="row mx-auto mb-3 col-12 col-md-8 col-lg-6 col-xl-4">
        <form class="input-group input-group-sm mb-3 mt-1 col-12 col-md-6" action="{{route('categories.search')}}" method="post">
            @csrf
            <div class="input-group-prepend">
                <button class="input-group-text" id="inputGroup-sizing-sm">Buscar</button>
            </div>
            <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
        </form>
        <div class="col-12 col-md-4">
            <a href="{{route('categories.index')}}"><button class="btn btn-outline-dark w-100">Volver</button></a>
        </div>
    </div>
    <div class="table" style="overflow-x:auto;">
        <table class="mx-auto">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Descripcion</th>
                    <th scope="col" colspan="2">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($categories as $category => $data)
                <tr>
                    <th scope="row">{{$data->id}}</th>
                    <th>{{$data->name}}</th>
                    <th>{{$data->description}}</th>
                    <th><a href="{{route('categories.edit',['category'=>$data])}}"><i class="far fa-edit" title="editar"></i></a></th>
                    <th><a href="{{route('categories.delete',['id'=>$data->id])}}" data-method="delete"><i class="far fa-trash-alt" title="borrar"></i></a></th>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection