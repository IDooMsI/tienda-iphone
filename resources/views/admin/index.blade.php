@extends('layouts.app')
@section('content')
<div class="container">
    <div id="div-admin-buttons" class="row mt-3">
        <div class="col-3">
            <a href="{{route('categories.index')}}"><button class="btn btn-outline-dark">Categorias</button></a>
        </div>
        <div class="col-3">
            <a href="{{route('products.index')}}"><button class="btn btn-outline-dark">Productos</button></a>
        </div>
    </div>
</div>
@endsection