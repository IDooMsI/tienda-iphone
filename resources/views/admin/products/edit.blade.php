@extends('layouts.app')
@section('content')
<div class="col-12">
    <div class="col-12 text-center mb-5">
        <h2>Modificar Producto</h2>
    </div>
    <form class="col-12 mx-auto" action="{{route('products.update',['product'=>$product])}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="form-row">
            <div id="name" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label class="form-label" for="">Nombre</label></h4>
                <input name="name" class="form-control" type="text" value="{{ $product->name }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Nombre del nuevo producto</b></small>
                @error('name')
                <div id="error" class="alert alert-danger mx-auto col-12 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="category" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Categoria</label></h4>
                <select class="form-control mb-1" name="category" value="{{ $product->category->name }}" id="">
                    <option value="">Elija una categoria</option>
                    @foreach($categories as $category)
                    <option <?php if ($category->id == $product->category_id) echo "selected"; ?> value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Categoria a la que pertenece el producto</b></small>
                <a href="{{route('categories.create')}}"><button class="btn btn-outline-dark" type="button">Agregar Categoria</button></a>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Si no encuentra la categoria en el listado, agreguela</b></small>
                @error('category_id')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="subcategory" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Subcategoria</label></h4>
                <select class="form-control mb-1" name="subcategory" value="" id="">
                    <option value="">Elija una subcategoria</option>
                    @if($subcategories)
                    @foreach($subcategories as $subcategory)
                    <option <?php if ($subcategory->id == $product->subcategory_id) echo "selected"; ?> value="{{$subcategory->id}}">{{$subcategory->name}}</option>
                    @endforeach
                    @else
                    <option value="{{$subcategory->id}}">{{$subcategory->name}}</option>
                    @endif
                </select>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Solo para telefonos</b></small>
                @error('subcategory_id')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="category_repuestos" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Categoria Repuestos</label></h4>
                <select class="form-control mb-1" name="category_repuestos" value="" id="">
                    <option value="">Elija una Categoria de repuesto</option>
                    <option <?php if ($product->categories_repuestos == "modulos") echo "selected"; ?> value="modulos">Modulos</option>
                    <option <?php if ($product->categories_repuestos == "baterias") echo "selected"; ?> value="baterias">Baterias</option>
                    <option <?php if ($product->categories_repuestos == "pin de carga") echo "selected"; ?> value="pin de carga">Pin de Carga</option>
                    <option <?php if ($product->categories_repuestos == "camaras") echo "selected"; ?> value="camaras">Camaras</option>
                    <option <?php if ($product->categories_repuestos == "carcasas") echo "selected"; ?> value="carcasas">Carcasas</option>
                </select>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Solo para Respuestos</b></small>
            </div>
            <div id="price" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Precio</label></h4>
                <input name="price" class="form-control" type="number" value="{{ $product->price }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Precio del producto Ej: 10500,50</b></small>
                @error('price')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="discount" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Descuento</label></h4>
                <input name="sale" class="form-control" type="number" value="{{ $product->sale }}">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Solo poner el numero que corresponde al % Ej: 25</b></small>
                @error('sale')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="div-color" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Color</label></h4>
                <input class="" id="input-color" name="codigo" onchange="getColorName()" type="color" value="{{ $product->color->codigo }}">
                <input id="input-name-color" name="name-color" value="{{$product->color->name}}" readonly></input>
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Elija un color para el producto</b></small>
                @error('color')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="image" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label for="">Imagen</label></h4>
                <input name="img[]" multiple class="form-control-file" type="file">
                <small id="passwordHelpBlock" class="form-text text-muted"><b>Elija una imagen para su producto</b></small><br>
                @error('image')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
            <div id="extras" class="form-gruop col-12 col-md-6 col-lg-3">
                <h4><label for="">Extras</label></h4>
                <div class="form-check">
                    @if($product->destacado)
                    <input name="destacado" class="mt-1 mr-1" type="checkbox" checked value="si">
                    <label for="">Producto destacado</label><br>
                    @else
                    <input name="destacado" class="mt-1 mr-1" type="checkbox" value="si">
                    <label for="">Producto destacado</label><br>
                    @endif
                </div>
                <div class="form-check">
                    @if($product->destacado)
                    <input name="stock" class="mt-1 mr-1" type="checkbox" checked value="si">
                    <label for="">Stock</label>
                    @else
                    <input name="stock" class="mt-1 mr-1" type="checkbox" value="si">
                    <label for="">Stock</label>
                    @endif
                </div>
            </div>
            <div id="description" class="form-group col-12 col-md-6 col-lg-3">
                <h4><label class="" for="">Descripcion</label></h4>
                <textarea class="form-control" name="description" maxlength="300" cols="30" rows="5" onkeyup="countChars(this);">{{$product->description}}</textarea>
                <p id="charNum">300 caracteres restantes</p>
                @error('description')
                <div id="error" class="alert alert-danger mx-auto col-11 col-sm-4 col-lg-12"><span>{{ $message }}</span></div>
                @enderror
            </div>
        </div>
        <div id="buttons" class="text-center col-12">
            <button class="mx-2 btn btn-outline-dark" type="submit">Modificar</button>
            <a href="{{route('products.index')}}"><button class="btn btn-outline-dark" type="button">Cancelar</button></a>
        </div>
    </form>
</div>
@endsection