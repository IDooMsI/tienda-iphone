@extends('layouts.app')
@section('content')
<div id="div-all-prod" class="col-12">
    <div id="div-tit-prod" class="col-12">
        <h2>Productos</h2>
        <a href="{{route('admin')}}" style="text-decoration: none; color:black"><i class="fas fa-arrow-circle-left"></i> Volver</a>
    </div>
    <div id="div-opc-prod" class="row mx-auto mb-3 col-12 col-md-8 col-lg-6 col-xl-4">
        <div id="div-but-new-index-product" class="my-3 col-6 col-md-4">
            <a href="{{route('products.create')}}"><button class="btn btn-outline-dark w-100">Nuevo</button></a>
        </div>
        <form class="input-group input-group-sm my-auto mb-3 col-12 col-md-6" action="{{route('products.search')}}" method="post">
            @csrf
            <div class="input-group-prepend">
                <button class="input-group-text" id="inputGroup-sizing-sm">Buscar</button>
            </div>
            <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
        </form>
    </div>
    <div class="my-2 col-12 text-center">
        @if(Session::has('notice'))
        <h3 class="my-auto text-success"><strong>{{ Session::get('notice') }}</strong></h3>
        @endif
    </div>
</div>
<div class="table" style="overflow-x:auto;">
    <table class="mx-auto">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Stock</th>
                <th scope="col">Precio</th>
                <th scope="col">Descuento</th>
                <th scope="col">Destacado</th>
                <th scope="col">Categoria</th>
                <th scope="col">Subcategoria</th>
                <th scope="col">Categoria repuesto</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Color</th>
                <th scope="col">Imagen</th>
                <th scope="col" colspan="2">Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product => $data)
            <tr>
                <th scope="row">{{$data->id}}</th>
                <th class="text-center">{{$data->name}}</th>
                @if ($data->stock)
                <th class="text-center">Si</th>
                @else
                <th class="text-center">No</th>
                @endif
                <th class="text-center">{{$data->price}}</th>
                @if($data->sale)
                <th class="text-center">{{$data->sale}}%</th>
                @else
                <th class="text-center">No tiene</th>
                @endif
                @if($data->destacado)
                <th class="text-center">Si</th>
                @else
                <th class="text-center">No</th>
                @endif
                <th class="text-center">{{$data->category->name}}</th>
                @if($data->subcategory)
                <th class="text-center">{{$data->subcategory->name}}</th>
                @else
                <th class="text-center">No tiene</th>
                @endif
                @if($data->categories_repuestos)
                <th class="text-center">{{$data->categories_repuestos}}</th>
                @else
                <th class="text-center">No tiene</th>
                @endif
                <th class="text-center">{{Str::limit($data->description,20)}}</th>
                <th class="text-center">{{$data->color->name}}</th>
                <th id="th-index-product" class="text-center"><img id="img-product" src="{{asset('storage/'.$data->image)}}" alt="imagen de: {{$data->name}}"></th>
                <th><a href="{{route('products.edit',['product'=>$data->id])}}"><i class="far fa-edit" title="editar"></i></a></th>
                <th><a href="{{route('products.delete',['id'=>$data->id])}}"><i class="far fa-trash-alt" title="borrar"></i></a></th>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection