@extends('layouts.app')
@section('content')
<div id="div-results-prod" class="col-12">
    <div id="div-tit-prod" class="col-12">
        <h2 class="">Resultados de busqueda</h2>
    </div>
    <div id="div-cancel-prod" class="row mx-auto mb-3 col-12 col-md-8 col-lg-6 col-xl-4">
        <div class="mx-auto my-3 col-6 col-md-4">
            <a href="{{route('products.index')}}"><button class="btn btn-outline-dark w-100">Volver</button></a>
        </div>
        <form class="input-group input-group-sm my-auto mb-3 col-12 col-md-6" action="{{route('products.search')}}" method="post">
            @csrf
            <div class="input-group-prepend">
                <button class="input-group-text" id="inputGroup-sizing-sm">Buscar</button>
            </div>
            <input type="text" class="form-control" aria-label="Small" aria-describedby="inputGroup-sizing-sm">
        </form>
    </div>
    <div class="table" style="overflow-x:auto;">
        <table class="mx-auto">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Descuento</th>
                    <th scope="col">Destacado</th>
                    <th scope="col">Usado</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Descripcion</th>
                    <th scope="col">Imagen</th>
                    <th scope="col" colspan="2">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product => $data)
                <tr>
                    <th scope="row">{{$data->id}}</th>
                    <th class="text-center">{{$data->name}}</th>
                    <th class="text-center">{{$data->price}}</th>
                    <th class="text-center">{{$data->sale}}</th>
                    <th class="text-center">{{$data->destacado}}</th>
                    <th class="text-center">{{$data->used}}</th>
                    <th class="text-center">{{$data->category->name}}</th>
                    <th class="text-center">{{$data->description}}</th>
                    <th id="th-index-product" class="text-center"><img id="img-product" src="{{asset('storage/'.$data->image)}}" alt="imagen de: {{$data->name}}"></th>
                    <th><a href="{{route('products.edit',['product'=>$data])}}"><i class="far fa-edit" title="editar"></i></a></th>
                    <th><a href="{{route('products.delete',['id'=>$data->id])}}"><i class="far fa-trash-alt" title="borrar"></i></a></th>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection