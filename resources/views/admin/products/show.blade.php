@extends('layouts.app')
@section('content')
<div class="col-12">
    <div class="row justify-content-around">
        <div class="col-10 col-md-5">
            <img class="w-100" src="{{asset('storage/'.$product->image)}}" alt="{{$product->name}}">
        </div>
        <div class="col-12 col-md-7">
            <div class="col-12 my-3">
            <small><a class="text-reset" href="/category/{{$product->category->name}}">{{$product->category->name}}</a> > {{$product->name}}</small>
                <h1>
                    <strong>
                        {{$product->name." - "}}
                    </strong>
                    <small class="text-muted">
                        {{$product->color->name}}
                    </small>
                </h1>
            </div>
            <div class="col-12 my-3">
                <h3>
                    $ {{$product->price}}
                </h3>
            </div>
            <div class="col-md-8 col-12 my-3">   
                    <p class="text-break">
                    {{$product->description}}
                </p>
            </div>
            <div class="col-8 my-2">
                <h6>Metodos de pago:</h6>
                <img class="w-100" src="{{asset('/storage/tarjetas.jpg')}}" alt="">
            </div>
            <div class="col-12 col-md-8 my-2">
                 
                <a id="bwp" class="my-3" href="javascript:;">
                    <svg class="icon-arrow before">
                        <use xlink:href="#arrow"></use>
                        </svg>
                        <span class="label">Coordinar compra <i class="fas fa-shopping-cart "></i></span>
                        <svg class="icon-arrow after">
                            <use xlink:href="#arrow"></use>
                        </svg>
                    </a>

                    <svg style="display: none;">
                        <defs>
                            <symbol id="arrow" viewBox="0 0 35 15">
                        <title>Arrow</title>
                        <path d="M27.172 5L25 2.828 27.828 0 34.9 7.071l-7.07 7.071L25 11.314 27.314 9H0V5h27.172z "/>
                        </symbol>
                    </defs>
                    </svg>              
            </div>
        </div>
    </div>
</div>
@endsection