@extends('layouts.app')
@section('content')
<div class="row justify-content-around">
    <div class="col-10">
        <!-- <div class="row my-3">
            @if(isset($subcategory))
            <div class="col-10 my-3">
                <h4><a href="{{route('index')}}" class="text-reset text-decoration-none">Home</a>
                    <span>></span>
                    <a href="{{route('telefonos')}}" class="text-reset text-decoration-none">Telefonos</a>
                    <span>></span>
                    <a href="{{route('telefonos.subcategory',['subcategory'=>$subcategory->name])}}" class="text-reset text-decoration-none">{{ucfirst($subcategory->name)}}</a>
                </h4>
            </div>
            @elseif(isset($category))
            <div class="col-10 mt-3">
                <h4><a href="{{route('index')}}" class="text-reset text-decoration-none">Home</a>
                    <span>></span>
                    <a href="{{route('productos',['category'=>$category->name])}}" class="text-reset text-decoration-none">{{ucfirst($category->name)}}</a>
                </h4>
            </div>
            @else
            <div class="col-10 mt-3">
                <h4><a href="{{route('index')}}" class="text-reset text-decoration-none">Home</a>
                    <span>></span>
                    <a href="{{route('telefonos')}}" class="text-reset text-decoration-none">Telefonos</a>
                </h4>
            </div>
            @endif
        </div> -->
        <div id="icons-reparaciones" class=" my-3 row justify-content-center">
            <div class="col-1 ml-1">
                <div class="row">
                    <a class="w-100 text-center bg-dark text-white p-2" href="{{route('repuestos.category',['category'=>'modulos'])}}" style="text-decoration:none">
                        <i class="fas fa-2x fa-mobile-alt"></i>
                        <span>Modulos</span>
                    </a>
                </div>
            </div>
            <div class="col-1 mx-5">
                <div class="row">
                    <a class="w-100 text-center bg-dark text-white p-2" href="{{route('repuestos.category',['category'=>'baterias'])}}" style="text-decoration:none">
                        <i class="fas fa-2x fa-battery-empty"></i>
                        <span>Baterias</span>
                    </a>
                </div>
            </div>
            <div class="col-1">
                <div class="row">
                    <a class="w-100 text-center bg-dark text-white p-2" href="{{route('repuestos.category',['category'=>'pin de carga'])}}" style="text-decoration:none">
                        <i class="fas fa-2x fa-bolt"></i><br>
                        <span style="font-size:91%">Pin de carga</span>
                    </a>
                </div>
            </div>
            <div class="col-1 mx-5">
                <div class="row">
                    <a class="w-100 text-center bg-dark text-white p-2" href="{{route('repuestos.category',['category'=>'camaras'])}}" style="text-decoration:none">
                        <i class="fas fa-2x fa-camera"></i>
                        <span>Camaras</span>
                    </a>
                </div>
            </div>
            <div class="col-1">
                <div class="row">
                    <a class="w-100 text-center bg-dark text-white p-2" href="{{route('repuestos.category',['category'=>'carcasas'])}}" style="text-decoration:none">
                        <i class="fas fa-2x fa-mobile"></i>
                        <span>Carcasas</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row justify-content-left">
            @foreach ($products as $product)
            <div class="col-md-4 col-lg-3 col-sm-6 my-2  text-center">
                <div>
                    <a href="{{route('products.show',['id'=>$product])}}"><img class="border border-ligth w-100" src="{{ asset ('storage/'.$product->image) }}" alt="{{$product->name}}"></a>
                </div>
                <div>
                    <span>{{$product->name}} - <small class="text-muted">{{$product->color->name}}</small></span>
                    @if ($product->stock)
                        <span class="text-success">Hay stock</span>
                    @else
                        <span class="text-danger">Sin stock</span>
                    @endif
                </div>
                @if (isset($product->sale))
                <div>
                    <span>$ {{$product->discount}}</span>
                    <span class="bg-success text-light small p-1 mx-auto my-auto">{{$product->sale}}% OFF</span>
                </div>
                @else
                <div>
                    <span>$ {{$product->price}}</span>
                </div>
                @endif
            </div>
            @endforeach
        </div>
    </div>
    @endsection