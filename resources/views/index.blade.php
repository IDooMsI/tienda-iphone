@extends('layouts.app')
@section('content')
<div class="col-12 justify-content-center">
  <div class="mt-5 text-center">
    <h1 id="subtit-index-cat"><strong>PRODUCTOS DESTACADOS</strong></h1>
  </div>
  <div class="col-12 mx-auto">
    <div class="row justify-content-left">
      @foreach ($products as $product)
      <div class="col-4 col-md-3  my-2 text-center">
        <div>
          <a href="products/{{$product->id}}"><img class="w-75" src="{{ asset ('storage/'.$product->image) }}" alt="{{$product->name}}"></a>
        </div>
        <div>
          <h4><span>{{$product->name}}</h4>
          <small>Color </small> - <small class="text-muted">{{$product->color->name}}</small></span>
        </div>
        @if (isset($product->sale))
            <div>
                <span>$ {{$product->discount}}</span>
                <span class="bg-success text-light small p-1 mx-auto my-auto">{{$product->sale}}% OFF</span>
            </div>   
            @else
            <div>
                <span>$ {{$product->price}}</span>
                
            </div>
            @endif
      </div>
      @endforeach
    </div>
  </div>
</div>
<style>

</style>
@endsection