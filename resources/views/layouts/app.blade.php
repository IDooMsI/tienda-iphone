<?php

use App\Category;
use App\Subcategory;

$categories = Category::all();
$telefono = $categories[0];
unset($categories[0]);
$subcategories = Subcategory::all();


?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Tienda-Iphone</title>
    <link rel="Shortcut Icon" href="{{asset('storage/logonegroconfondo.png')}}" type="image/x-icon" />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-dark shadow">
            <div class="row">
                <div class="col-8 col-md-4 col-lg-3">
                    <a class="navbar-brand " href="{{ url('/') }}">
                        <img src="{{asset('/storage/logo-blanco.png')}}" class="w-100 " alt="image-logo">
                    </a>
                </div>
                <button class="navbar-toggler ml-auto bg-white mr-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse col-10 col-md-8" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ucfirst($telefono->name)}}</a>
                            <div class="dropdown-menu text-dark bg-light col-2 p-2" aria-labelledby="navbarDropdown2">
                                @foreach($telefono->subcategories as $subcategory)
                                <a class="nav-link text-left p-1" style="color:black !important;" href="{{route('telefonos.subcategory',['subcategory'=>$subcategory->name])}}">{{ucfirst($subcategory->name)}}</a>
                                @endforeach
                            </div>
                        </li>
                        @foreach($categories as $category)
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="{{route('productos',['category'=>$category->name])}}">{{ucfirst($category->name)}}</a>
                        </li>
                        @endforeach
                        @auth
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            @if(Auth::user()->admin == 325)
                            <div class="dropdown-menu dropdown-menu-right text-dark bg-light" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" style="color:black !important;" href="{{route('admin')}}">Panel</a>
                                <a class="dropdown-item" style="color:black !important;" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                @endif
                            </div>
                        </li>
                        @endauth
                    </ul>
                    <form class="input-group input-group-sm mb-4 my-auto col-12 col-md-4 col-lg-3 p-0" action="{{route('buscar')}}" method="get">
                        @csrf
                        @method('POST')
                        <input type="text" name="buscador" class="form-control" placeholder="Buscador de productos">
                        <div class="input-group-prepend">
                            <button class="btn  btn-outline-light"><i class="fas fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </nav>
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{asset('storage/banner1.png')}}" alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('storage/banner2.png')}}" alt="Second slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('storage/banner3.png')}}" alt="Third slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('storage/banner4.png')}}" alt="Third slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <main class="">
            @yield('content')
        </main>
        <footer class="footer mt-auto pt-2 pb-0 bg-dark">
            <div class="col-12 mx-auto">
                <div class="row justify-content-around">
                    <div class="col-md-4 col-lg-4 col-12 p-3 order-2 text-center align-self-center ">
                        <h5><i class="far fa-clock " style="color: rgba(255, 255, 255, 0.753)"></i><span style="color: rgba(255, 255, 255)"> Lunes a Viernes 16:00 - 20:30</span></h5>
                        <h5><i class="fas fa-envelope" style="color: rgba(255, 255, 255, 0.753)"></i> <a href="mailto:info@tiendaiphone.com" class="text-white">info@tiendaiphone.com</a></h5>
                        <h5><i class="fab fa-facebook" style="color: rgba(255, 255, 255, 0.753)"></i> <a href="https://www.facebook.com/Tienda.iphoneoficial1/" class="text-white"> @Tienda.iphoneoficial1</a></h5>
                        <h5><i class="fab fa-instagram" style="color: rgba(255, 255, 255, 0.753)"></i> <a href="https://www.instagram.com/1tienda.iphone/?igshid=1g9q2c4ufllbv" class="text-white">@1tienda.iphone</a></h5>
                        <h5><i class="fas fa-globe" style="color: rgba(255, 255, 255, 0.753)"></i> <a href="https://tienda-iphone.com/" target="_blank" class="text-white">www.tienda-iphone.com</a></h5>
                    </div>
                    <div class="col-md-8 col-lg-4 col-12 p-3 order-3 text-center align-self-center">
                        <i class="fas fa-md fa-map-marker-alt" style="color: rgba(255, 255, 255, 0.753)" ></i> <a href="https://www.google.com/maps/search/?api=1&query=-34.806000,-58.281700">Doctor Pedro Boruel 2890</a><br>
                        <a href="https://www.google.com/maps/search/?api=1&query=-34.806000,-58.281700"><img class="w-75 pt-2" src="{{ asset ('storage/map.jpeg') }}" alt=""></a>
                    </div>
                    <div class="col-md-3 col-lg-4 d-none d-lg-block d-xl-block p-3 order-1 text-center align-self-center">
                        <img class="w-75" src="{{ asset ('storage/logo-blanco.png') }}" alt="">
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{asset('js/category.js') }}"></script>
    <script src="{{asset('js/product.js') }}"></script>
    <script src="{{asset('js/show.js') }}"></script>
    <script src="{{asset('js/admin.js') }}"></script>
    <script src="{{asset('js/choices.js') }}"></script>
</body>

</html>