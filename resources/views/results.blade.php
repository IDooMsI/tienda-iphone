@extends('layouts.app')
@section('content')
<div class="row justify-content-around">
    <div class="col-10">
        <div class="col-12 text-center my-3">
            <h3>Resultados de busqueda</h3>
        </div>
        <div class="row justify-content-left">
            @foreach ($products as $product)
            <div class="col-md-4 col-lg-3 col-sm-6 my-2  text-center">
                <div>
                    <a href="/products/{{$product->id}}"><img class="border border-ligth w-100" src="{{ asset ('storage/'.$product->image) }}" alt="{{$product->name}}"></a>
                </div>
                <div>
                    <span>{{$product->name}} - <small class="text-muted">{{$product->color->name}}</small></span>
                </div>
                @if (isset($product->sale))
                <div>
                    <span>$ {{$product->discount}}</span>
                    <span class="bg-success text-light small p-1 mx-auto my-auto">{{$product->sale}}% OFF</span>
                </div>
                @else
                <div>
                    <span>$ {{$product->price}}</span>
                </div>
                @endif
            </div>
            @endforeach
        </div>
    </div>
    @endsection