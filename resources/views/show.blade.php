@extends('layouts.app')
@section('content')
<div class="col-12">
    <div class="row justify-content-around">
        <!--esta es la pagina original-->
        <div class="col-10 col-md-5">
            <img id="principal" class="w-100" src="{{asset('storage/'.$product->imagen[0]->name)}}" alt="{{$product->name}}">
            @if($product->imagen)
            <div class="row" style="margin-left: 35% !important; ">
                @foreach ($product->imagen as $image)
                <img id="{{$image->id}}" class="marco" onclick="choice(this.id)" style="width:20%; height:15%" src="{{asset('storage/'.$image->name)}}" alt="{{$product->name}}">
                @endforeach
            </div>
            @endif
        </div>
        <div class="mr-auto col-12 col-md-6">
            <div class="col-12 my-3">
                <h1><strong id="phone">{{$product->name}}</strong><span class="text-muted"> - {{$product->color->name}}</span></h1>
            </div>
            <div class="col-12 my-3">
                @if (isset($product->sale))
                <span class="text-muted" style="text-decoration: line-through">
                    $ {{$product->price}}
                </span>
                <h3>
                    $ {{$product->discount}} &nbsp <span class="small text-success">{{$product->sale}}% OFF</span>
                </h3>
                @else
                <h3>
                    $ {{$product->price}}
                </h3>
                @endif
            </div>
            <div class="col-12 ">
                @if ($product->stock)
                <h6 class="bold text-success">Hay stock</h6>
                @else
                <h6 class="bold text-danger">Sin stock</h6>
                @endif
            </div>
            <!-- <div class="h-25 col-md-8 col-12 my-3">
                <p class="text-break">
                    {{$product->description}}
                </p>
            </div> -->
            <div class="col-8 my-2">
                <h6>Metodos de pago:</h6>
                <img class="w-100" src="{{asset('/storage/tarjetas.jpg')}}" alt="">
            </div>
            <div class="col-12 col-md-8 mt-auto">
                <a id="bwp" class="my-3" onclick="whatsapp()" href="javascript:;">
                    <svg class="icon-arrow before">
                        <use xlink:href="#arrow"></use>
                    </svg>
                    <span class="label">Coordinar compra <i class="fas fa-shopping-cart "></i></span>
                    <svg class="icon-arrow after">
                        <use xlink:href="#arrow"></use>
                    </svg>
                </a>
                <svg id="svg-bwp" style="display: none;">
                    <defs>
                        <symbol id="arrow" viewBox="0 0 35 15">
                            <title>Arrow</title>
                            <path d="M27.172 5L25 2.828 27.828 0 34.9 7.071l-7.07 7.071L25 11.314 27.314 9H0V5h27.172z " />
                        </symbol>
                    </defs>
                </svg>
            </div>
        </div>
        <div>
            <div class="h-25 col-md-8 col-12 my-3">
                <p class="text-break">
                    {{$product->description}}
                </p>
            </div>
        </div>
    </div>
</div>
@endsection