    <?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');



Auth::routes();

Route::group(['middleware'=> 'auth'],function(){
    
    Route::get('/admin', function () {
        return view('admin/index');
    })->name('admin');
    
    Route::get('/categories/{id}/delete','CategoryController@destroy')->name('categories.delete');
    Route::resource('categories', 'CategoryController');
    Route::post('/categories/search', 'CategoryController@search')->name('categories.search');
    
    Route::get('/products/{id}/delete', 'ProductController@destroy')->name('products.delete');
    Route::resource('products', 'ProductController'); 
    Route::post('/products/search', 'ProductController@search')->name('products.search');
});


Route::get('products/{id}','ProductController@show')->name('products.show');

Route::get('/telefonos/{subcategory}/show', 'TiendaController@showSubcat')->name('telefonos.subcategory');
Route::get('/telefonos','TiendaController@ShowAll')->name('telefonos');

Route::get('repuestos','TiendaController@showRepuestosAll')->name('repuestos.all');
Route::get('repuestos/{category}','TiendaController@showRepuestos')->name('repuestos.category');

Route::match(['post','get'],'/productos/buscar','TiendaController@buscarProductos')->name('buscar');
Route::get('/productos/{category}', 'TiendaController@showCat')->name('productos');
